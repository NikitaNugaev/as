package ru.nn.accountservice.startup;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import ru.nn.accountservice.other.Helper;

/**
 *
 * @author nikita
 */
@Singleton
@Startup
public class Initialization {

    @Inject
    Helper helper;

    @PostConstruct
    private void init() {
        helper.init(1);
        helper.init(2);
    }
}
