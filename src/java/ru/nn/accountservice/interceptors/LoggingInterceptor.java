package ru.nn.accountservice.interceptors;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.Interceptor;
import javax.interceptor.InvocationContext;
import ru.nn.accountservice.other.Helper;

/**
 *
 * @author nikita
 */
@Interceptor
@StatisticCollector
public class LoggingInterceptor {

    @Inject
    private Helper helper;

    @AroundInvoke
    public Object logMethod(InvocationContext ic) throws Exception {
//        System.out.println(ic.getMethod().getName());
        if ("getAmount".equals(ic.getMethod().getName())) {
            helper.test(1);
        }
        if ("addAmount".equals(ic.getMethod().getName())) {
            helper.test(2);
        }
        try {
            return ic.proceed();
        } finally {
        }
    }

}
