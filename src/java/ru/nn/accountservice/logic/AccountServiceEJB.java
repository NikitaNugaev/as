package ru.nn.accountservice.logic;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;
import ru.nn.accountservice.interceptors.StatisticCollector;
import ru.nn.accountservice.model.BankAccount;
import ru.nn.accountservice.target.AccountService;

/**
 *
 * @author nikita
 */
@StatisticCollector
@Stateless
public class AccountServiceEJB implements AccountService {

    @PersistenceContext(unitName = "AccountServicePU")
    EntityManager em;

    @Override
    public Long getAmount(Integer id) {
        BankAccount bankAccount;
        if ((bankAccount = em.find(BankAccount.class, id)) != null) {

            return bankAccount.getValue();
        } else {
            return 0L;
        }

    }

    @Override
    public void addAmount(Integer id, Long value) {

        BankAccount bankAccount;
        if ((bankAccount = em.find(BankAccount.class, id, LockModeType.PESSIMISTIC_WRITE)) != null) {
            bankAccount.setValue(bankAccount.getValue() + value);
            em.merge(bankAccount);
        } else {
            bankAccount = new BankAccount();
            bankAccount.setId(id);
            bankAccount.setValue(value);
            em.persist(bankAccount);
        }

    }

}
