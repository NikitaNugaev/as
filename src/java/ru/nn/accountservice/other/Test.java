package ru.nn.accountservice.other;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author nikita
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Test.REMOVE_ALL, query = "DELETE FROM Test t")
})
public class Test implements Serializable {

    public static final String REMOVE_ALL = "ru.nn.accountservice.other.Test.removeAll";

    @Id
    private Integer id;
    private Long value;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getValue() {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

}
