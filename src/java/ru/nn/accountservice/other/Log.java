package ru.nn.accountservice.other;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author nikita
 */
@Entity
@NamedQueries({
    @NamedQuery(name = Log.REMOVE_LOG, query = "DELETE FROM Log l")
})
public class Log implements Serializable {

    public static final String REMOVE_LOG = "ru.nn.accountservice.other.Log.removeAll";
    @Id
    @GeneratedValue
    private Long id;

    private String description;
    private Long quantity;

    @Temporal(TemporalType.TIME)
    private Date date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
