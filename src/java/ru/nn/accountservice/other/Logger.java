package ru.nn.accountservice.other;

import java.util.Date;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author nikita
 */
@Stateless
public class Logger {

    @PersistenceContext(unitName = "AccountServicePU")
    EntityManager em;
    private Long lastValueOfGetAmount = 0L;
    private Long lastValueOfAddAmount = 0L;

    @Schedule(hour = "*", minute = "*", second = "*/10")
    public void generateCommonReport() {
        Test test;
        if ((test = em.find(Test.class, 1)) != null) {
            Log log = new Log();
            log.setDate(new Date());
            log.setDescription("Общее количество запросов от всех клиентов для getAmount");
            log.setQuantity(test.getValue());
            em.persist(log);
        }
        if ((test = em.find(Test.class, 2)) != null) {
            Log log = new Log();

            log.setDate(new Date());
            log.setDescription("Общее количество запросов от всех клиентов для addAmount");
            log.setQuantity(test.getValue());
            em.persist(log);
        }

    }

    @Schedule(hour = "*", minute = "*", second = "*/1")
    public void generateReport() {
        Test test;
        if ((test = em.find(Test.class, 1)) != null) {
            Log log = new Log();
            log.setDate(new Date());
            log.setDescription("Количество запросов обрабатываемых за одну секунду для getAmount");
            log.setQuantity(test.getValue() - lastValueOfGetAmount);
            lastValueOfGetAmount = test.getValue();
            em.persist(log);
        }
        if ((test = em.find(Test.class, 2)) != null) {
            Log log = new Log();
            log.setDate(new Date());
            log.setDescription("Количество запросов обрабатываемых за одну секунду для addAmount");
            log.setQuantity(test.getValue() - lastValueOfAddAmount);
            lastValueOfAddAmount = test.getValue();
            em.persist(log);
        }
    }

//    @Schedule(hour = "*", minute = "*/15")
//    public void removeStats() {
//        em.createNamedQuery(Test.REMOVE_ALL).executeUpdate();
//        em.createNamedQuery(Log.REMOVE_LOG).executeUpdate();
//        
//    }
}
