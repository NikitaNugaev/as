package ru.nn.accountservice.other;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.PersistenceContext;

/**
 *
 * @author nikita
 */
@Stateless
public class Helper {

    @PersistenceContext(unitName = "AccountServicePU")
    EntityManager em;

    public void init(Integer id) {
        Test test;
        if ((test = em.find(Test.class, id)) == null) {
            test = new Test();
            test.setId(id);
            test.setValue(0L);
            em.persist(test);
        }
    }

    public void test(Integer id) {
        Test test = em.find(Test.class, id);
        em.lock(test, LockModeType.PESSIMISTIC_WRITE);
        test.setValue(test.getValue() + 1);
        em.merge(test);
    }
}
